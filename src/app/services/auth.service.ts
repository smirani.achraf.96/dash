import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  public getToken(): string {
    return localStorage.getItem('token');
  }

  login(email: string, password: string) {
    return this.http
      .post<any>('http://localhost:3000/admin/auth', { email, password })
      .pipe(
        map(result => {
          if (result && result.data) {
            localStorage.setItem('token', JSON.stringify(result.data.token));
            localStorage.setItem('currentUser', JSON.stringify(result.data.admin));
          }
          return result;
        })
      );
  }
}
