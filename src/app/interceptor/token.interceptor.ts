import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}

  intercept( request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.getToken()) {
        const tokenFiltered = this.auth.getToken().replace('"', '').replace('"', '');
      request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + tokenFiltered) });
  }

  if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
  }
  request = request.clone({ headers: request.headers.set('Access-Control-Allow-Origin', '*') });

  return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
          }
          return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
            reason: error , /*&& error.error.reason ? error.error.reason : ''*/
            status: error.status
        };
        return throwError(error);
    })
  );

  }
}
