import { Component, OnInit } from "@angular/core";

import * as $ from 'jquery'
import { FormGroup, FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: "app-blogs",
  templateUrl: "./blogs.component.html",
  styleUrls: ["./blogs.component.scss"]
})
export class BlogsComponent implements OnInit {
  blogEdit: any;
  blogAdd: any;
  blogs: any[];

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.blogEdit = new FormGroup({
      title: new FormControl(''),
      body: new FormControl('')
    });
    this.blogAdd = new FormGroup({
      title: new FormControl(''),
      body: new FormControl('')
    });
  }

  ngOnInit() {
    this.http.get("http://localhost:3000/blog/all").subscribe((blogs: any[]) => {
      this.blogs = blogs;
      console.log(blogs)
    })
  }

  removeBackdrop() {
    setTimeout(function () {
      $('.modal-backdrop').remove();
    }, 0);
  }

  createBlog() {
    this.http.post("http://localhost:3000/blog/add", this.blogAdd.value).subscribe(res => {
      this.refresh();
      console.log(res)
    })
  }

  patchValues(blogID) {
    this.http.get("http://localhost:3000/blog/" + blogID).subscribe((blog: any) => {
      this.blogEdit.patchValue({
        title: blog.title,
        body: blog.body
      })
    })
  }

  update(blogID) {
    this.http.put('http://localhost:3000/blog/update', this.blogEdit.value).subscribe(res => {
      this.refresh();
    })
  }

  remove(blogID) {
    this.http.delete('http://localhost:3000/blog/remove/' + blogID).subscribe(res => {
      this.refresh();
    })
  }

  refresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    const currentUrl = this.router.url + '?';
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }
}
