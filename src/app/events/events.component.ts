import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

import * as $ from 'jquery'
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: "app-events",
  templateUrl: "./events.component.html",
  styleUrls: ["./events.component.scss"]
})
export class EventsComponent implements OnInit {
  eventEdit: any;
  eventAdd: any;

  events: any[];
  categories: any[];

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.eventEdit = new FormGroup({
      titre: new FormControl(''),
      lieu: new FormControl(''),
      categorie: new FormControl(''),
      detail: new FormControl('')
    });
    this.eventAdd = new FormGroup({
      titre: new FormControl(''),
      lieu: new FormControl(''),
      categorie: new FormControl(''),
      detail: new FormControl('')
    });
  }

  ngOnInit() {
    this.setScripts();

    this.http.get("http://localhost:3000/evenement/all").subscribe((events: any[]) => {
      this.events = events
    })

    this.http.get("http://localhost:3000/categorie/all").subscribe((categories: any[]) => {
      this.categories = categories;
    })

  }

  setScripts() {
    this.loadScript('assets/vendors/js/datatables/pdfmake.js');
    this.loadScript('assets/vendors/js/datatables/datatables.js');
    this.loadScript('assets/vendors/js/datatables/dataTables.buttons.js');
    this.loadScript('assets/vendors/js/datatables/jszip.js');
    this.loadScript('assets/vendors/js/datatables/buttons.html5.js');
    this.loadScript('assets/vendors/js/datatables/vfs_fonts.js');
    this.loadScript('assets/vendors/js/datatables/buttons.print.js');
    this.loadScript('assets/js/components/tables/tables.js');
  }

  public loadScript(url: string) {
    const body = document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  removeBackdrop() {
    setTimeout(function () {
      $('.modal-backdrop').remove();
    }, 0);
  }

  createEvent() {
    this.http.post("http://localhost:3000/evenement/add", this.eventAdd.value).subscribe(res => {
      this.refresh();
    })
  }

  patchValues(eventID) {
    this.http.get("http://localhost:3000/evenement/" + eventID).subscribe((event: any) => {
      this.eventEdit.patchValue({
        id: event._id,
        titre: event.titre,
        detail: event.detail,
        lieu: event.lieu,
        categorie: event.lieu
      })
    })
  }

  update(eventID){
    this.http.put('http://localhost:3000/evenement/update', this.eventEdit.value).subscribe(res => {
        this.refresh();
    })
}

  remove(eventID) {
    this.http.delete('http://localhost:3000/evenement/remove/' + eventID).subscribe(res => {
      this.refresh();
    })
  }

  refresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    const currentUrl = this.router.url + '?';
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }
}
