import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import * as $ from 'jquery'
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-benevoles',
  templateUrl: './benevoles.component.html',
  styleUrls: ['./benevoles.component.scss']
})
export class BenevolesComponent implements OnInit {
  userEdit: any;
  userAdd: any;
  users: any[];

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.userEdit = new FormGroup({
      nom: new FormControl(''),
      prenom: new FormControl(''),
      type: new FormControl(''),
      email: new FormControl(''),
      ville: new FormControl('')
    });
    this.userAdd = new FormGroup({
      nom: new FormControl(''),
      prenom: new FormControl(''),
      type: new FormControl(''),
      email: new FormControl(''),
      ville: new FormControl(''),
      login: new FormControl(''),
      mot_de_passe: new FormControl('')
    });
  }

  ngOnInit() {
    this.setScripts();

    this.http.get("http://localhost:3000/benevole/all").subscribe((users: any[]) => {
      this.users = users;
    })

  }

  setScripts() {
    this.loadScript('assets/vendors/js/datatables/pdfmake.js');
    this.loadScript('assets/vendors/js/datatables/datatables.js');
    this.loadScript('assets/vendors/js/datatables/dataTables.buttons.js');
    this.loadScript('assets/vendors/js/datatables/jszip.js');
    this.loadScript('assets/vendors/js/datatables/buttons.html5.js');
    this.loadScript('assets/vendors/js/datatables/vfs_fonts.js');
    this.loadScript('assets/vendors/js/datatables/buttons.print.js');
    this.loadScript('assets/js/components/tables/tables.js');
  }

  public loadScript(url: string) {
    const body = document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  removeBackdrop() {
    setTimeout(function () {
      $('.modal-backdrop').remove();
    }, 0);
  }

  createUser() {
    this.http.post("http://localhost:3000/benevole/add", this.userAdd.value).subscribe(res => {
      this.refresh();
    })
  }

  patchValues(userID) {
    this.http.get("http://localhost:3000/benevole/" + userID).subscribe((user: any) => {
      this.userEdit.patchValue({
        id: user._id,
        nom: user.nom,
        prenom: user.prenom,
        login: user.login,
        type: user.Type,
        email: user.email,
        ville: user.ville
      })
    })
  }

  update(userID) {
    this.http.put('http://localhost:3000/benevole/update', this.userEdit.value).subscribe(res => {
      this.refresh();
    })
  }

  remove(userID) {
    this.http.delete('http://localhost:3000/benevole/remove/' + userID).subscribe(res => {
      this.refresh();
    })
  }

  refresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    const currentUrl = this.router.url + '?';
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

}
