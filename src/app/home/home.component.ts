import { Component, OnInit } from '@angular/core';
import {ChartType, LegendItem} from '../lbd/lbd-chart/lbd-chart.component';
import {EventService} from '../services/event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public data: any;
  public emailChartType: ChartType;
  public emailChartData: any;
  public emailChartLegendItems: LegendItem[];
  public citoyenPercent: any;
  public benevolePercent: any;
  public formations: Number = 0;
  public articles: Number = 0;
  public events: Number = 0;
  public projets: Number = 0;


  constructor() { }

  ngOnInit() {

    this.data = JSON.parse(localStorage.getItem('stats'));
    this.citoyenPercent = (this.data.citoyen/(this.data.citoyen+this.data.benevole)) * 100;
    this.benevolePercent = (this.data.benevole/(this.data.citoyen+this.data.benevole)) * 100;
    this.emailChartType = ChartType.Pie;
    this.formations = this.data.formation;
    this.articles = this.data.articles;
    this.events = this.data.events;
    this.projets = this.data.projects;
    this.emailChartData = {
      labels: [Math.round(this.citoyenPercent)+'%', Math.round(this.benevolePercent)+'%'],
      series: [Math.round(this.citoyenPercent), Math.round(this.benevolePercent)]
    };
    this.emailChartLegendItems = [
      { title: 'Citoyen', imageClass: 'fa fa-circle text-info' },
      { title: 'Benevole', imageClass: 'fa fa-circle text-danger' }
    ];
  }

}
