import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { TokenInterceptor } from './interceptor/token.interceptor';
import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { SigninComponent } from './signin/signin.component';

@NgModule({
  imports: [
      BrowserAnimationsModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule,
      HttpModule,
      HttpClientModule,
      NavbarModule,
      FooterModule,
      SidebarModule,
      AppRoutingModule,

      JwtModule
  ],
  declarations: [
      AppComponent,
      AdminLayoutComponent,
      SigninComponent,
    // UserComponent,
    // UsersComponent,
    // FormationComponent,
    // LevelsComponent,
    // SectionsComponent,
    // MajorsComponent,
    // DocumentsComponent,
    // CorrectionsComponent,
    // SubjectsComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
