import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {EventService} from '../services/event.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor( private authService: AuthService, private router: Router, private statsService: EventService) { }

  ngOnInit() {
  }

  onSubmit(event) {
    event.preventDefault();
    const target = event.target;
    const email = target.querySelector('#email').value;
    const password = target.querySelector('#password').value;
    this.authService.login(email, password).subscribe(() => {
      this.statsService.getStats().subscribe((res) => {
        localStorage.setItem('stats', JSON.stringify(res))
      });
      this.router.navigate(['/dashboard']);
    });
  }

}
