import { Component, OnInit } from "@angular/core";

import * as $ from 'jquery'
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
@Component({
  selector: "app-groups",
  templateUrl: "./groups.component.html",
  styleUrls: ["./groups.component.scss"]
})
export class GroupsComponent implements OnInit {
  groupEdit: any;
  groupAdd: any;
  groups: any[];

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.groupEdit = new FormGroup({
      nom: new FormControl('')
    });
    this.groupAdd = new FormGroup({
      nom: new FormControl('')
    });
  }

  ngOnInit() {
    this.http.get("http://localhost:3000/groupe/all").subscribe((groups: any[]) => {
      this.groups = groups;
    })
  }

  removeBackdrop() {
    setTimeout(function () {
      $('.modal-backdrop').remove();
    }, 0);
  }

  createGroup() {
    this.http.post("http://localhost:3000/groupe/add", this.groupAdd.value).subscribe(res => {
      this.refresh();
    })
  }

  patchValues(groupID) {
    this.http.get("http://localhost:3000/groupe/" + groupID).subscribe((group: any) => {
      this.groupEdit.patchValue({
        title: group.title,
        body: group.body
      })
    })
  }

  update(groupID) {
    this.http.put('http://localhost:3000/groupe/update', this.groupEdit.value).subscribe(res => {
      this.refresh();
    })
  }

  remove(groupID) {
    this.http.delete('http://localhost:3000/groupe/remove/' + groupID).subscribe(res => {
      this.refresh();
    })
  }

  refresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    const currentUrl = this.router.url + '?';
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }
}
