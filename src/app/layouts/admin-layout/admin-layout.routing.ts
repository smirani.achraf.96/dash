import { Routes } from '@angular/router';

import { HomeComponent } from '../../home/home.component';
import { EventsComponent } from '../../events/events.component';
import { BlogsComponent } from '../../blogs/blogs.component';
import { GroupsComponent } from '../../groups/groups.component';
import { BenevolesComponent } from 'app/benevoles/benevoles.component';
import { CitoyensComponent } from 'app/citoyens/citoyens.component';


export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: HomeComponent },
  { path: 'citoyens', component: CitoyensComponent},
  { path: 'benevoles', component: BenevolesComponent },
  { path: 'events', component: EventsComponent },
  { path: 'blogs', component: BlogsComponent },
  { path: 'groups', component: GroupsComponent },
  { path: 'signout', redirectTo: 'signin', }
];
