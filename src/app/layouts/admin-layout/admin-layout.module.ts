import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LbdModule } from '../../lbd/lbd.module';
import { NguiMapModule} from '@ngui/map';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent } from '../../home/home.component';

import { CitoyensComponent } from '../../citoyens/citoyens.component';
import { EventsComponent } from '../../events/events.component';
import { BlogsComponent } from '../../blogs/blogs.component';
import { GroupsComponent } from '../../groups/groups.component';
import {MatFormFieldModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import { BenevolesComponent } from 'app/benevoles/benevoles.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    LbdModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule
  ],
  declarations: [
    HomeComponent,
    CitoyensComponent,
    EventsComponent,
    BlogsComponent,
    GroupsComponent,
    BenevolesComponent
  ]
})

export class AdminLayoutModule {}
