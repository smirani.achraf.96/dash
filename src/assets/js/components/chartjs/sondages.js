var canvas = document.getElementById('myChart');
var data = {
	labels: ["Oui", "Non", "Ok"],
	datasets: [
		{
			label: "Label",
			backgroundColor: ["#08a6c3", "#5cb85c", "#d9534f"],
			hoverBorderColor: ["#fff", "#fff", "#fff"],
			borderColor: ["#fff", "#fff", "#fff"],
			borderWidth: 10,
			data: [2478, 4268, 1265],
		}
	]
};

function adddataAll(){
	myLineChart.data.datasets[0].data = [1000, 4068, 265];
	myLineChart.update();
}
function adddataFemale(){
	myLineChart.data.datasets[0].data = [2000, 68, 265];
	myLineChart.update();
}
function adddataMale(){
	myLineChart.data.datasets[0].data = [3000, 488, 265];
	myLineChart.update();
}
function adddataAge1(){
	myLineChart.data.datasets[0].data = [4000, 4068, 265];
	myLineChart.update();
}
function adddataAge2(){
	myLineChart.data.datasets[0].data = [5000, 4068, 265];
	myLineChart.update();
}
function adddataAge3(){
	myLineChart.data.datasets[0].data = [5500, 4068, 265];
	myLineChart.update();
}

var option = {
	legend: {
		display: true,
		position: 'top',
		labels: {
			fontColor: "#2e3451",
			fontSize: 14
		}
	},
	tooltips: {
		backgroundColor: 'rgba(47, 49, 66, 0.8)',
		titleFontSize: 13,
		titleFontColor: '#fff',
		caretSize: 0,
		cornerRadius: 4,
		xPadding: 10,
		displayColors: true,
		yPadding: 10
	}
};
var myLineChart = Chart.Doughnut(canvas,{
	data:data,
	options:option
});




$(document).ready(function(){
	$('.btn-chart').click(function(){
		$('.btn-chart').removeClass('bg-grey');
		$(this).addClass('bg-grey');
	});
})