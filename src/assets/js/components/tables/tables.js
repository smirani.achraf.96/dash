(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	


	$(function () {
		$('#sorting-table').DataTable({
			"language": {
				"lengthMenu": "Afficher _MENU_ par page",
				"zeroRecords": "Nothing found - sorry",
				"info": "Page _PAGE_ de _PAGES_",
				"infoEmpty": "Aucun résultat trouvé",
				"zeroRecords": "Rien trouvé - Désolé",
				"search": "Chercher",
				"infoFiltered": "(total: _MAX_ )",
				"paginate": {
					"previous": "Précédent",
					"next": "Suivant"
				}
			},
			"lengthMenu": [
				[5, 10, 15, -1],
				[5, 10, 15, "Tous"]
			],
			"order": [
				[0, "desc"]
			],
			dom: 'lBfrtip',
			buttons: {
				buttons: [{
					attr: { id: 'printButton' },
					extend: 'print',
					text: '<i class="la la-print"></i>',
					title: $('h1').text(),
					exportOptions: {
						columns: [ 0, 1, 2, 3]
					},
					footer: true,
					autoPrint: true,
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: ''
					}
				}
			}
		});
	});


	$(function moveButton() {
		$("#printButton").prependTo("#printOption");
		setTimeout(moveButton, 500);
	});


	$(function () {
		$('#export-table').DataTable({
			dom: 'Bfrtip',
			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});
	});

})(jQuery);