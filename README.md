## Terminal Commands

1. Run in terminal: ```npm install -g @angular/cli```
2. Then: ```npm install```
3. ``` npm rebuild node-sass ```
4. And: ```ng serve```
5. Navigate to `http://localhost:4200/`

## Template Preview

https://demos.creative-tim.com/light-bootstrap-dashboard-angular2/dashboard[link]
